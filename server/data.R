sdata = reactive({
  if (input$exchk)  {
    show("sdatapbdiv")
    updateProgressBar(session=session, id = "sdatapb", value = 0, title = "Loading..")
    sdata = load_sampling_results("test_data/", "group1")
    #print("done")
    updateProgressBar(session=session, id = "sdatapb", value = 100, title = "Done!")
    for (i in 1:length(sdata)){
      output[[paste0('T', i)]] = renderDataTable({sdata[[i]]}, options = list(
        pageLength = 5,
        autoWidth = T,
        scrollX=T), rownames=F)
      }
    sdata
  } else{
    req(input$sdatain)
    show("sdatapbdiv")
    updateProgressBar(session=session, id = "sdatapb", value = 0, title = "Loading..")
    ntables = length(input$sdatain[, 1])
    sdata = list()
    names_l = c()
    for(intable in 1:ntables){
      name.split = unlist(str_split(file_path_sans_ext(input$sdatain$name[intable], compression = T), "_"))
      name = if (name.split[length(name.split)] != "WT") "MUT" else "WT"
      names_l = c(names_l, name)
      sdata[[intable]] = fread(input$sdatain[[intable, 'datapath']])
      updateProgressBar(session=session, id = "sdatapb", value = (intable/ntables)*100, title = "Loading..")
      output[[paste0('T', intable)]] =
        renderDataTable({sdata[[intable]]}, options = list(
          pageLength = 5,
          autoWidth = T,
          scrollX=T), rownames=F)
    }
    names(sdata) = names_l
    sdata
    # print("Done")
  }
})

# Disable file upload if using example data
observeEvent(input$exchk, {
  if (input$exchk) {
    shinyjs::disable("sdatain")
    sdata()
  }
  else {
    shinyjs::enable("sdatain")}
})

# Necessary to load sdatain
observe({
  req(input$sdatain)
  sdata()})

# Datatables to show once you've uploaded files
observeEvent(list(input$datachk, input$exchk), {
  if (input$datachk) show("tables") else hide("tables")
})
output$tables = renderUI({
  req(isTruthy(sdata()) || isTruthy(input$exchk))
  tagList(lapply(1:length(sdata()), function(i) {
    dataTableOutput(paste0('T', i))
  }))
})

# Metab dict
m_dict = reactive({
  if (input$reconchk) {
    read.csv("test_data/metab_dict.tsv", sep = "\t")
  } else if (!is.null(input$metabdict$datapath)){
    read.csv(input$metabdict$datapath, sep = "\t")
  }else{
    NULL
  }
})

# Scenario dict
s_dict = reactive({
  if (input$scenariochk) {
    read.csv("test_data/scenario_dict.tsv", sep = "\t")
  } else if (!is.null(input$scenariodict$datapath)){
    read.csv(input$scenariodict$datapath, sep = "\t")
  }else{
    NULL
  }
})

# metab_dict_exists = eventReactive(c(input$metabdict,input$reconchk), {
#   if(!is.null(input$metabdict$datapath) | input$reconchk){
#     NULL
#   }
# })

observeEvent(input$reconchk, {
  if (input$reconchk) shinyjs::disable("metabdict") else shinyjs::enable("metabdict")
})
observeEvent(input$metabchk, {
  if (input$metabchk) show("metabdicttable") else hide("metabdicttable")
})

observeEvent(input$scenariochk, {
  if (input$scenariochk) shinyjs::disable("scenariodict") else shinyjs::enable("scenariodict")
})
observeEvent(input$scenariotablechk, {
  if (input$scenariotablechk) show("scenariodicttable") else hide("scenariodicttable")
})

observeEvent(input$zexchk, {
  if (input$zexchk) shinyjs::disable("zdatain") else shinyjs::enable("zdatain")
})
observeEvent(input$zscorechk, {
  if (input$zscorechk) show("inzscoretable") else hide("inzscoretable")
})
output$metabdicttable = renderDataTable({
  req(isTruthy(input$metabdict) || isTruthy(input$reconchk))
  datatable(m_dict(), options = list(
    autoWidth = T,
    scrollX=T), rownames=F)
})
output$scenariodicttable = renderDataTable({
  req(isTruthy(input$scenariodict) || isTruthy(input$scenariochk))
  datatable(s_dict(), options = list(
    autoWidth = T,
    scrollX=T), rownames=F)
})

# Zscore data
zscoresdata = reactive({
  if (input$zexchk)  {
    read.table("test_data/zscores.tsv", sep="\t", header=T)
  }else{
    read.table(input$zdatain$datapath, sep="\t", header=T)
  }
})
output$inzscoretable = renderDataTable({
  req(isTruthy(input$zdatain)|| isTruthy(input$zexchk))
  datatable(zscoresdata(), options = list(
    autoWidth = T,
    scrollX=T), rownames=F)
})
#
# Density data
rdata = reactive({
  if (input$rdsexchk)  {
    readRDS("test_data/densities.rds")
  }else{
    req(input$rdatain)
    #cat(file=stderr(),input$rdatain$datapath)
    show("rdatapbdiv")
    updateProgressBar(session=session, id = "rdatapb", value = 0, title = "Loading..")
    readRDS(input$rdatain$datapath)
    updateProgressBar(session=session, id = "rdatapb", value = 100, title = "Done!")
  }
})
output$rdatatable = renderDataTable({
  req(isTruthy(input$rdatain)|| isTruthy(input$rdsexchk))
  rnames = as.data.frame(names(rdata()))
  colnames(rnames) = c("Condition names")
  datatable(rnames, options = list(
    autoWidth = T,
    scrollX=T), rownames=F)
})
observeEvent(input$rdatachk, {
  if (input$rdatachk) show("rdatatable") else hide("rdatatable")
})
